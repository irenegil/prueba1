class Cuadrado: 
    """ Un ejemplo de clase para los Cuadrado"""
    def __init__ (self, l=1):
        self.lado = l
        
    def calculo_perimetro(self):
        return self.lado*2

    def calculo_area(self):
        return self.lado**2
        
print("El perimetro del cuadrado es {}m²". format(calculo_perimetro))
